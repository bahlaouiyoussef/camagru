-- https://dev.mysql.com/doc/refman/5.7/en/grant.html;
-- CREATE USER 'camagru'@'localhost' IDENTIFIED BY 'toor';
-- GRANT ALL ON *.* TO 'camagru'@'localhost';

DROP DATABASE IF EXISTS `db_camagru`;
CREATE DATABASE IF NOT EXISTS `db_camagru`;

CREATE TABLE IF NOT EXISTS `db_camagru`.`users` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`firstname` VARCHAR(50),
	`lastname` VARCHAR(50),
	`username` VARCHAR(100) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`creation_date` DATETIME NOT NULL,
	`is_active` BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`posts` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`owner_id` INT NOT NULL,
	`creation_date` DATETIME NOT NULL,
	`text` TEXT DEFAULT '' NOT NULL
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`images` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`owner_id` INT NOT NULL,
	`creation_date` DATETIME NOT NULL,
	`filename` VARCHAR(256)
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`post_images` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`post_id` INT NOT
	`image_id` INT NOT NULL
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`comments` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`owner_id` INT NOT NULL,
	`post_id` INT NOT NULL,
	`text` TEXT NOT NULL,
	`creation_date` DATETIME NOT NULL
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`user_reactions` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`owner_id` INT NOT NULL,
	`post_id` INT NOT NULL,
	`reaction_id` INT NOT NULL,
	CONSTRAINT UK_owner_id_post_id_reaction_id UNIQUE(`owner_id`, `post_id`, `reaction_id`)
);

CREATE TABLE IF NOT EXISTS `db_camagru`.`reactions` (
	`id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	`label` VARCHAR(50) NOT NULL,
	CONSTRAINT UK_label UNIQUE(`label`)
);
