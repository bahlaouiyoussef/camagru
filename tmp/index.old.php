<?php
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

use App\Router\Router;
use App\Router\Route;
use App\Router\RouterException;

function __autoload($class)
{
	$class = str_replace('\\', '/', $class) . '.class.php';
	if (file_exists($class))
		include_once("$class");
}

$router = new Router($_GET['url']);

$router->get('/', function () {
	print("<h1>Home</h1>");
});

$router->get('/posts', function () {
	print("<h1>Posts</h1>");
});

$router->get('/posts/:id-:slug', function ($id, $slug) {
	print("<h1>Posts: $slug: $id</h1>");
})->with('id', '[0-9]+')->with('slug', '[a-zA-Z\-]+');

$router->get('/posts/:id', function ($id) {
?>
<form action="" method="post">
	<input type="text" name="name"/>
	<button type="submit">Send</button>
</form>
<?php
});

$router->post('/posts/:id', function ($id) {
	print('<pre>' . print_r($_POST, true) . '</pre>');
});

if ($router->start() === false)
{
	// header("HTTP/1.0 404 Not Found");
	print('404 not found');
}
