<?php
namespace Core;

abstract class Controller
{
	protected $_data;

	public function __construct()
	{
		$this->_data = [];
	}

	final public function loadModel($model)
	{
		$modelClass = "\\App\\Models\\${model}Model";
		return (new $modelClass());
	}

	final public function renderView($view, $data = [])
	{
		$filename = S_VIEWS . '/' . $view . '.view.php';
		if (file_exists($filename) === false)
			throw new ControllerException("Unknown View: `$view`");
		extract($this->_data);
		extract($data);
		ob_start();
		require(S_INCLUDES . '/navbar.inc.php');
		$navbar = ob_get_clean();
		ob_start();
		require(S_INCLUDES . '/footer.inc.php');
		$footer = ob_get_clean();
		ob_start();
		require($filename);
		$content = ob_get_clean();
		$pageTitle = $view;
		require(S_VIEWS . '/template.view.php');
	}

	final public function renderApi($api)
	{
		$filename = S_APIS . '/' . $api . '.api.php';
		if (file_exists($filename) === false)
			throw new ControllerException("Unknown Api: `$api`");
		extract($this->_data);
		require($filename);
	}
}
