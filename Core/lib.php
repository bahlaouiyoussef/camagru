<?php
function hash_password($password)
{
	return (hash('sha256', $password, false));
}

function json_data($error, $message, $data)
{
	return (json_encode([
		'error' => $error,
		'message' => $message,
		'data' => $data
	]));
}

function generate_token(array $strings)
{
	$time = time();
	$input = '';
	foreach ($strings as $string)
		$input .= $string;
	$str = hash_password($input . $time);
	return ($str);
}

function generate_filename($username, $size = 15, $extention = 'png')
{
	$username = str_shuffle($username);
	return (uniqid($username, true) . '.' . $extention);
	$random_str = '';
	$charset = '0123456789abcdefghijklmnopqrstuvwxyz-_';
	$max = strlen($charset) - 1;
	if ($username === null)
		$username = 'random string';
	$username = str_shuffle($username);
	for ($i = 1; $i <= $size; $i++)
		$random_str .= $charset[rand(0, $max)];
	$random_str .= time();
	return (hash('sha256', $random_str . $username) . '.' . $extention);
}

function session_started()
{
	return (defined('SESSION_STARTED'));
}

function is_connected()
{
	if (!session_started() || !isset($_SESSION['user_data']))
		return (false);
	return (true);
}

function is_activated()
{
	if (!is_connected())
		return (false);
	$user_data = json_decode($_SESSION['user_data']);
	return ($user_data->is_active === 1);
}

function validate_email($email)
{
	$email = trim($email);
	return (filter_var($email, FILTER_VALIDATE_EMAIL));
}

function validate_password($pass)
{
	$len = strlen($pass);
	$char_count = 0;
	if ($len < 6 || $len > 20)
		return (false);
	for ($i = 0; $i < $len; $i++)
	{
		$c = $pass[$i];
		if (!is_numeric($c) && !ctype_alpha($c))
			$char_count++;
	}
	if ($char_count === 0)
		return (false);
	return ($pass);
}

function html_sanitize($html)
{
	return (htmlentities($html));	
}

function startsWith ($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
}

function validate_username($uname)
{
	$uname = trim($uname);
	$len = strlen($uname);
	if ($len == 0)
		return (false);
	for ($i = 0; $i < $len; $i++)
	{
		$c = $uname[$i];
		if (!is_numeric($c) && !ctype_alpha($c) && $c != '-' && $c != '_')
			return (false);
	}
	return ($uname);
}

function mergeImage($base64image, $filtername, &$errno)
{
	$errno = 1;
	$prefix = 'data:image/png;base64,';
	if (!startsWith($base64image, $prefix))
		return (false);
	$base64image = substr($base64image, strlen($prefix));
	$base64image = base64_decode($base64image);
	$image_src = @imagecreatefromstring($base64image);
	if ($image_src === false)
		return (false);
	$width = imagesx($image_src);
	$height = imagesy($image_src);
	if ($width <= 200 || $height <= 200)
	{
		$errno = 2;
		return (false);
	}
	$filter_src = imagecreatefrompng(S_FILTERS . '/' . $filtername);
	if ($filter_src === false)
		return (false);
	$result = imagecopy($image_src, $filter_src, 0, 0, 0, 0, 200, 200);
	if ($result === false)
		return (false);
	$errno = 0;
	return ($image_src);
}

function create_comments($model, $id, $id_owner = -1)
{
	$comments = $model->getComments($id);
	foreach ($comments as $comment):
	?>
	<div class="comment">
		<label class="owner"><?= $comment['username'] ?></label>
		<label class="date"><?= $comment['date'] ?></label>
		<p class="text"><?= html_sanitize($comment['text']) ?></p>
		<?php
		if ($comment['owner_id'] == $id_owner):
		?>
		<div class="delete" data-role="delete" data-id="<?= $comment['comment_id'] ?>"></div>
		<?php
		endif;
		?>
	</div>
	<?php
	endforeach;
}

function create_reactions($model, $image_id, $user_id)
{
	$infos = $model->getImageReactionsDetails($image_id, $user_id);
	$reactions = $model->getReactions();
	foreach ($infos as $key => $value):
	?>
	<label class="reaction <?= $value['has_current_user'] == true ? 'active' : '' ?>" data-react="<?= $value['reaction_id'] ?>" data-role="react" data-image="<?= $image_id ?>" >
		<div class="icon small" style="background-image: url('<?= C_ICONS . "/$key.png"?>')" >0</div>
		<label><?= $value['count'] ?></label>
	</label>
	<?php
	endforeach;
	foreach ($reactions as $key => $value):
		if (array_key_exists($key, $infos))
			continue ;
	?>
	<label class="reaction" data-react="<?= $value ?>" data-role="react" data-image="<?= $image_id ?>" >
		<div class="icon small" style="background-image: url('<?= C_ICONS . "/$key.png"?>')" >0</div>
		<label>0</label>
	</label>
	<?php
	endforeach;
}

function create_gallery_links($model)
{
	$imagesCount = $model->getImagesCount();
	$page = 1;
	?>
	<div class="pages_links">
	<?php
	for ($i = 1; $i <= $imagesCount; $i += MAX_IMAGES_PAGE):
	?>
		<a href="/gallery/<?= $page ?>"><?= $page ?></a>
	<?php
	$page++;
	endfor;
	?>
	</div>
	<?php
}
