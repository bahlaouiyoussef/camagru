<?php
namespace Core\Router;

class Route
{
	private $path;
	private $callbale;
	private $args;
	private $params;
	private $_action;

	public function __construct($path, $callbale, $action = null)
	{
		$this->path = trim($path, '/');
		$this->callable = $callbale;
		$this->_action = $action;
		$this->args = [];
		$this->params = [];
	}

	public function with($param, $pattern)
	{
		$this->params[$param] = $pattern;
		return ($this);
	}

	public function match($url)
	{
		$url = trim($url, '/');
		$path = preg_replace_callback('#:([\w]+)#', [$this, 'paramMatch'], $this->path);
		if (!preg_match("#^$path$#i", $url, $matches))
			return (false);
		array_shift($matches);
		$this->args = $matches;
		return (true);
	}

	public function call()
	{
		$value = null;
		if ($this->callable !== null)
			$value = call_user_func_array($this->callable, $this->args);
		if ($this->_action !== null)
		{
			$arr = explode('#', $this->_action);
			$controller = '\\App\\Controllers\\' . $arr[0] . 'Controller';
			$controller = new $controller();
			$method = $arr[1];
			$controller->$method($value);
		}
	}

	/*
	** private methodes
	*/
	private function paramMatch($matches)
	{
		if (isset($this->params[$matches[1]]))
			return ('(' . $this->params[$matches[1]] . ')');
		return ('([^/]+)');
	}
}
