<?php
namespace Core;
use \PDO;

class Database
{
	private $_pdo = null;
	private static $_instance = null;

	public static function init($dsn, $username, $password)
	{
		if (self::$_instance === null)
			self::$_instance = new Database($dsn, $username, $password);
		return (self::$_instance);
	}

	public function __construct($dsn, $username, $password)
	{
		$this->_pdo = new PDO($dsn, $username, $password);
		$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		$this->_pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	}

	public function setAttribute($attr, $value)
	{
		$this->_pdo->setAttribute($attr, $value);
	}

	/*
	** @param string $query query string.
	** @param array $params array of parameters.
	** @return boolean
	*/
	public function nonQuery($query, $params = [])
	{
		$stm = $this->_pdo->prepare($query);
		if ($stm === false)
			return (false);
		$ret = $stm->execute($params);
		$stm->closeCursor();
		return ($ret !== false);
	}

	/*
	** @return last insert id
	*/

	public function lastInsertId()
	{
		$id = $this->_pdo->lastInsertId();
		return ($id);
	}

	/*
	** @param string $query query string.
	** @param array $params array of parameters.
	** @return mixed
	*/
	public function selectQuery($query, $params = [])
	{
		$stm = $this->_pdo->prepare($query);
		if ($stm === false)
			return (false);
		$error = $stm->execute($params) === false;
		if ($error === false)
			$result = $stm->fetchAll();
		$stm->closeCursor();
		if ($error === true)
			return (true);
		return ($result);
	}
}
