<?php
$model = new \App\Models\UserModel();
$user_id = 0;
if (is_connected())
	$user_id = json_decode($_SESSION['user_data'])->id;
$css_links = [
	'gallery.css'
];
$post_id = 0;
?>
<div id="container posts">
	<?php create_gallery_links($model); ?>
	<?php
	foreach ($images as $image):
	?>
	<div class="post">
		<div class="image">
			<img src="<?= C_USERS_IMGS . '/' . $image['filename'] ?>" alt="user image" />	
			<div class="footer">
				<div class="left">
					<div class="col-10 details">
					<?php create_reactions($model, $image['id'], $user_id); ?>
					</div>
				</div>
				<?php
				if (is_connected()):
				?>
				<div class="right">
					<button class="button fill" data-role="comment" data-id="<?= $image['id'] ?>" data-post-id="#_<?= $post_id ?>" >Comment</button>
				</div>
				<?php
				endif;
				?>
			</div>
		</div>
		<div class="controls">
			<div data-id="#_<?= $post_id ?>" class="comment-action down" data-role="comment-action"></div>
		</div>
		<div id="_<?= $post_id ?>" class="comments">
			<?php create_comments($model, $image['id'], $user_id); ?>
		</div>
	</div>
	<?php
	$post_id++;
	endforeach;
	?>
</div>
<div id="comment_modal" class="modal">
	<div class="container modal-inner">
		<div class="control">
			<textarea id="input_comment" class="textarea fill"></textarea>
		</div>
		<div class="control">
			<button id="btn_save" class="button fill">Save</button>
		</div>
	</div>
</div>
<script src="<?= C_JS . '/' . 'gallery.js' ?>"></script>