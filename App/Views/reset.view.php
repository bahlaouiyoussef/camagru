<?php
$email = htmlentities($email);
$token = htmlentities($token);
?>
<section class="container form">
	<input type="hidden" id="input_email" value="<?= $email ?>" />
	<input type="hidden" id="input_token" value="<?= $token ?>" />
	<div id="error">
	</div>
	<section class="body">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<div class="control">
			<label class="label">New password</label>
			<input class="textbox" id="input_pass" type="password" />
		</div>
		<div class="control">
			<button class="button" id="btn_reset">Reset</button>
		</div>
	</section>
</section>
<script src="<?= C_JS ?>/reset.js"></script>