<section class="container form">
	<div id="error">
	</div>
	<section class="body">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<div class="control">
			<label class="label">Email</label>
			<input class="textbox" id="input_email" type="text" value="<?= $user_data->email ?>" />
		</div>
		<div class="control">
			<label class="label">Username</label>
			<input class="textbox" id="input_uname" type="text" value="<?= $user_data->username ?>" />
		</div>
		<div class="control">
			<label class="label">New password</label>
			<input class="textbox" id="input_newpass" type="password" />
		</div>
		<div class="control">
			<label class="label">Password</label>
			<input class="textbox" id="input_pass" type="password" />
		</div>
        <div class="control">
            <label class="label">Activate e-mail notification</label>
            <br />
            <label class="switch-toggle outer">
				<input id="input_notif" type="checkbox" value="notif" <?= $user_data->notification ? 'checked' : '' ?> />
				<div></div>
			</label>
        </div>
		<div class="control">
			<button class="button" id="btn_update">Update</button>
		</div>
	</section>
</section>
<script src="<?= C_JS ?>/profile.js"></script>