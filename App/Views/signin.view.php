<section class="container form">
	<div id="error">
	</div>
	<section class="body">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<div class="control">
			<label class="label">Username</label>
			<input tabindex="1" class="textbox" id="input_uname" type="text" />
		</div>
		<div class="control">
			<label class="label">Password</label>
			<input tabindex="2" class="textbox" id="input_pass" type="password" />
		</div>
		<div class="control">
			<a tabindex="4" class="label link" href="/sendreset">Forgot password ?</a>
		</div>
		<div class="control">
			<button tabindex="3" class="button" id="btn_signin">Signin</button>
		</div>
	</section>
</section>
<script src="<?= C_JS ?>/signin.js"></script>