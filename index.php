<?php
define('SESSION_STARTED', session_start());
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

use Core\Router\Router;
use Core\Router\Route;
use Core\Router\RouterException;
use App\Controllers\UserController;
use App\Models\UserModel;
use Core\Database;
require_once('config/init.php');
require_once(S_CORE . '/lib.php');

function __autoload($class)
{
	$class = str_replace('\\', '/', $class) . '.class.php';
	// echo $class . '<br/>';
	if (file_exists($class))
		include_once("$class");
}

$router = new Router($_GET['url']);

$router->get('/', function () use ($router) {
	$router->redirect('/signin');
});

if (!isset($_SESSION['user_data']))
	$_SESSION['user_data'] = null;

$router->get('/signin', function () use ($router) {
	if (is_connected())
		$router->redirect('/gallery/1');
}, 'User#signinView');

$router->get('/signout', function () use ($router) {
	$_SESSION['user_data'] = null;
	$router->redirect('/signin');
});

$router->get('/signup', function () use ($router) {
	if (is_connected())
		$router->redirect('/gallery/1');
}, 'User#signupView');

$router->get('/gallery', function () use ($router) {
	$router->redirect('/gallery/1');
});

$router->get('/gallery/:page', function ($page) use ($router) {
	if (is_connected() && !is_activated())
		$router->redirect('/activate');
	return (intval($page));
}, 'User#galleryView');

$router->get('/create', function () use ($router) {
	if (!is_connected())
		$router->redirect('/signin');
	else if (!is_activated())
		$router->redirect('/activate');
}, 'User#createView');

$router->get('/profile', function () use ($router) {
	if (!is_connected())
		$router->redirect('/signin');
	else if (!is_activated())
		$router->redirect('/activate');
}, 'User#profileView');

$router->get('/activate', function () use ($router) {
	if (!is_connected())
		$router->redirect('/signin');
	else if (is_activated())
		$router->redirect('/gallery');
}, 'User#activateView');

$router->get('/activate/:token', function ($token) use ($router) {
	if (is_connected())
		$router->redirect('/profile');
	else
	{
		$user = new UserController();
		if ($user->activateAccount($token) === true)
			print('Account activated. <a href="/signin">Signin</a>');
		else
			print('Can\'t activate your account.');
	}
});

$router->get('/sendreset', function ()  use ($router) {
	if (is_connected())
		$router->redirect('/profile');
}, 'User#sendResetView');

$router->get('/reset/:email/:token', function ($email, $token) {
	if (is_connected())
		$router->redirect('/profile');
	return ([ 'email' => $email, 'token' => $token ]);
}, 'User#resetView');

$router->post('/delete', null, 'User#deleteImage' );

$router->post('/getimages', null, 'User#getImages');

$router->post('/signin', null, 'User#signin');

$router->post('/signup', null, 'User#signup');

$router->post('/react', null, 'User#react');

$router->post('/comment', null, 'User#comment');

$router->post('/deletecomment', null, 'User#deleteComment');

$router->post('/update', null, 'User#update');

$router->post('sendresetrequest', null, 'User#sendResetRequest');

$router->post('reset', null, 'User#reset');

$router->post('/upload', function () use ($router) {
	if (!is_connected())
		$router->redirect('/signin');
	else if (!is_activated())
		$router->redirect('/activate');
	else
	{
		$message = 'something went wrong';
		$user_data = json_decode($_SESSION['user_data']);
		$userModel = new UserModel();
		$error = false;
		if (!file_exists(S_USERS_IMGS))
			$error = !mkdir(S_USERS_IMGS);
		if ($error === false)
			$error = !isset($_POST['image']) || !isset($_POST['filter']);
		if ($error === false)
		{
			$base64image = $_POST['image'];
			$filter = $userModel->getFilterById(intval($_POST['filter']));
			$error = $filter === false;
			if ($error === false)
			{
				$image_src = mergeImage($base64image, $filter['filename'], $errno);
				$error = $image_src === false;
				if ($error === false)
				{
					$filename = generate_filename($user_data->username);
					$error = !imagepng($image_src, S_USERS_IMGS . '/' . $filename);
					if ($error === false)
						$error = !$userModel->insertUserImage($filename);
				}
				else if ($errno === 2)
					$message = 'images size must be greater than 200px * 200px';
			}
		}
		if ($error === true)
			print(json_data(1, $message, ''));
		else
			print(json_data(0, 'success', $userModel->getImages($user_data->id)));
	}
});

$router->get('/filter/:name', function ($name) {
	header('Content-Type: image/png');
	$filename = S_FILTERS . '/' . $name . '.png';
	if (file_exists($filename))
		readfile($filename);
});

$router->get('/setup', function () {
	require(S_CONFIG . '/setup.php');
});

if ($router->start() === false)
{
	$url = $_GET['url'];
	require(S_VIEWS . '/404.php');
}
