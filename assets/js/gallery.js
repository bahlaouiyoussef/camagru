dom(window).load(function() {
	const btn_save = dom('#btn_save');
	const input_comment = dom('#input_comment');

	btn_save.click(function() {
		const id_image = dom(this).attr('data-id');
		const $this = dom(this);

		if (isNull(id_image))
			return;
		AJAX.post({
			'url': '/comment',
			'params': { 'image_id': id_image, 'text': input_comment.val() },
			'load': function(event) {
				var data;

				try {
					input_comment.val('');
					dom('#comment_modal').removeClass('show');
					data = JSON.parse(event.target.responseText);
					if (data.error === 0) {
						data = data.data.comment;
						dom($this.attr('data-post-id')).prepend(create_comment(data.text, data.username, data.date, data.comment_id, delete_handler));
					} else {
						// console.log(data.message);
					}
				} catch (e) {
					console.log(e.message);
				}
			}
		});
	});

	dom('[data-role="react"]').click(function() {
		const $this = dom(this);
		const react = $this.attr('data-react');
		const image = $this.attr('data-image');

		if (isNull(react) || isNull(image))
			return;
		AJAX.post({
			'url': '/react',
			'params': { 'reaction_id': react, 'image_id': image },
			'load': function(event) {
				try {
					const res = JSON.parse(event.target.responseText);
					if (res.error === 1)
						return;
					if (res.data.is_set == true)
						$this.addClass('active');
					else
						$this.removeClass('active');
					const label = $this.children('label');
					if (!isNull(label)) {
						var total = parseInt(label.text());
						total += res.data.is_set == true ? 1 : -1;
						label.text(total);
					}
				} catch (e) {
					console.log(e.message);
				}
			}
		});
	});

	dom('[data-role="comment"]').click(function() {
		const $this = dom(this);

		if (!isNull($this.attr('data-id'))) {
			btn_save.attr('data-id', $this.attr('data-id'));
			btn_save.attr('data-post-id', $this.attr('data-post-id'));
			dom('#comment_modal').addClass('show');
		}
	});

	dom('[data-role="delete"]').click(delete_handler);

	dom('#comment_modal').click(function(event) {
		if (event.target === this) {
			btn_save.removeAttr('data-id');
			dom(this).removeClass('show');
		}
	});

	dom('[data-role="comment-action"]').click(function (event) {
		try {
			const	$this = dom(this);
			const	id = $this.attr('data-id');
			if (isNull(id))
				return ;
			$this.toggleClass('down');
			const	post = dom(id);
			post.toggleClass('show');
		}
		catch (e) {
			console.log(e.message);
		}
	});
});

function delete_handler(event) {
	const	$this = dom(this);
	const	comment_id = $this.attr('data-id');

	if (isNull(comment_id))
		return ;
	AJAX.post({
		'url': '/deletecomment',
		'params': { 'comment_id': comment_id },
		'load': function (e) {
			try {
				const	data = JSON.parse(e.target.responseText);
				if (data.error === 0)
					event.target.parentNode.remove();
			} catch (e) {
				console.log(e.message);
			}
		}
	});
}
