dom('#btn_signin').click(function() {
    const uname = dom('#input_uname').val();
    const pass = dom('#input_pass').val();

    if (!isNull(uname) && !isNull(pass)) {
        AJAX.post({
            'url': '/signin',
            'params': { 'uname': uname, 'pass': pass },
            'load': function(event) {
                var data;
                try {
                    data = JSON.parse(event.target.responseText);
                    if (data.error === 0)
                        redirect('/gallery/1');
                    else
                        setMessage(data, '#message');
                } catch (e) {
                    console.log(e.message);
                }
            }
        });
    }
});