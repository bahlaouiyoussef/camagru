dom(window).load(function () {
    var input_email = dom('#input_email');
    var input_uname = dom('#input_uname');
    var input_pass = dom('#input_pass');
    var input_newpass = dom('#input_newpass');
    var input_notif = dom('#input_notif');

    dom('#btn_update').click(function () {
		var	email = input_email.val();
		var	uname = input_uname.val();
		var	pass = input_pass.val();
		var	newpass = input_newpass.val();
		var	notif = input_notif.prop('checked');

		if (isNull(email) || isNull(uname) || isNull(pass) || isNull(notif))
			return ;
        AJAX.post({
            'url': '/update',
            'params': {
				'email': email,
				'uname': uname,
				'pass': pass,
				'newpass': newpass,
				'notif': notif
			},
            'load': function (event) {
				try {
					// dom('#debug').html(event.target.responseText);
					const	res = JSON.parse(event.target.responseText);
					setMessage(res, '#message');
				} catch (e) {
					console.log(e.message);
				}
            }
        });
    });
});