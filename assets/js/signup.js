dom('#btn_signup').click(function () {
	const email = dom('#input_email').val();
	const uname = dom('#input_uname').val();
	const pass = dom('#input_pass').val();

	if (email !== undefined && uname !== undefined && pass !== undefined) {
		AJAX.post({
			'url': '/signup',
			'params': { 'email': email, 'uname': uname, 'pass': pass },
			'load': function (event) {
				try {
					const	res = JSON.parse(event.target.responseText);
					setMessage(res, '#message');
				} catch (e) {
					console.log(e.message);
				}
			}
		});
	}
});
