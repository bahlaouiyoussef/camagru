dom('#btn_reset').click(function() {
	const   email = dom('#input_email').val();

	if (!isNull(email)) {
		AJAX.post({
			'url': '/sendresetrequest',
			'params': { 'email': email },
			'load': function(event) {
				var data;
				try {
					data = JSON.parse(event.target.responseText);
					setMessage(data, '#message');
				} catch (e) {
					console.log(e.message);
				}
			}
		});
	}
});