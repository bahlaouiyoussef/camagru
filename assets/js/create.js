// https://developer.mozilla.org/fr/docs/Web/API/MediaDevices/getUserMedia
'use strict';
var		CURRENT_FILTER = null
var		CAN_TAKEPIC = true;
var		WIDTH = null;
var		HEIGHT = null;

dom(window).load(function () {
	const	video = dom('#video');
	const	image_to_upload = dom('#image_to_upload');

	navigator.mediaDevices.getUserMedia({ 'video': true })
	.then(function (mediaStream) {
		const	video_obj = document.getElementById('video');
		if ('srcObject' in video_obj)
			video.prop('srcObject', mediaStream);
		else
			video.prop('src', URL.createObjectURL(mediaStream));
		video.on({
			'loadedmetadata': function () {
				try {
					var settings = mediaStream.getTracks()[0].getSettings();
					WIDTH = settings.width;
					HEIGHT = settings.height;
				}
				catch (e) {
					WIDTH = 640;
					HEIGHT = 480;
				}
				video.play();
			}
		});
	})
	.catch(function () {
	});

	dom('#btn_capture').click(function () {
		if (CURRENT_FILTER === null)
			alert('Choose a filter first');
		else
		{
			dom('#btn_save').prop('disabled', false);
			createImageFromVideo('image_to_upload', 'video');
			video.removeClass('active');
			video.addClass('hidden');
			image_to_upload.removeClass('hidden');
			image_to_upload.addClass('active');
		}
	});

	dom('#upload_modal').click(function () {
		dom('#upload_modal').removeClass('show');
	});

	dom('#input_file').on({
		'change': function () {
			if (this.files.length === 1)
			{
				dom('#upload_modal').addClass('show');
				createImageFromFile(this, 'image_to_upload', function () {
					video.removeClass('active');
					video.addClass('hidden');
					image_to_upload.removeClass('hidden');
					image_to_upload.addClass('active');
				});
			}
		}
	});

	dom('#btn_save').click(function () {
		if (CURRENT_FILTER === null)
			alert('Choose a filter first');
		else
			uploadImage('image_to_upload');
	});

	dom('#btn_show').click(function () {
		dom('#side').addClass('active');
	});

	dom('#btn_close').click(function () {
		dom('#side').removeClass('active');
	});

	dom('#btn_cancel').click(function () {
		resetInputs();
		video.removeClass('hidden');
		video.addClass('active');
		image_to_upload.removeClass('active');
		image_to_upload.addClass('hidden');
	});

	dom('.filter').click(function (event) {
		const	vid = document.getElementById('video');
		dom('.filter').removeClass('active');
		this.classList.add('active');
		CURRENT_FILTER = this.getAttribute('data-id');
		if (!isNull(CURRENT_FILTER)) {
			dom('#btn_capture').prop('disabled', false);
		}
	});

	dom('[data-role="delete"]').click(delete_image_handler);
});

function createImageFromVideo(canvasId, videoId) {
	const	canvas = document.getElementById(canvasId);
	const	video = document.getElementById(videoId);
	canvas.width = WIDTH;
	canvas.height = HEIGHT;
	canvas.getContext('2d').clearRect(0, 0, WIDTH, HEIGHT);
	canvas.getContext('2d').drawImage(video, 0, 0, WIDTH, HEIGHT);
}

function createImageFromFile(input_file, canvasId, callable) {
	const	canvas = document.getElementById(canvasId);
	var		image = new Image();

	canvas.getContext('2d').clearRect(0, 0, WIDTH, HEIGHT);
	image.onload = function () {
		canvas.width = image.width;
		canvas.height = image.height;
		canvas.getContext('2d').drawImage(image, 0, 0, image.width, image.height);
		callable();
		dom('#btn_save').prop('disabled', false);
	}
	image.onerror = function () {
		setMessage({ 'error': 1, 'message': 'Cannot load image.' }, '#message');
	}
	image.src = URL.createObjectURL(input_file.files[0]);
}

function uploadImage(id) {
	const data = document.getElementById(id).toDataURL('image/png');
	AJAX.post({
		'url': '/upload',
		'params': { 'image': data, 'filter': CURRENT_FILTER },
		'load': function (event) {
			const	video = dom('#video');
			const	image_to_upload = dom('#image_to_upload');

			update_image_list(event.target.responseText);
			dom('#side').addClass('active');
			resetInputs();
			video.removeClass('hidden');
			video.addClass('active');
			image_to_upload.removeClass('active');
			image_to_upload.addClass('hidden');
		}
	});
}

function update_image_list(res) {
	try {
		res = JSON.parse(res);
		if (res.error === 0) {
			if (res.data.length === 0)
				return ;
			const	image = res.data[0];
			const	elm = create_image(image.filename, image.id, delete_image_handler);
			dom('#list_images').prepend(elm);
		}
		setMessage(res, '#message');
	}
	catch (e) {
		console.log(e.message);
		return (false);
	}
}

function resetInputs() {
	CURRENT_FILTER = null;
	dom('.filter').removeClass('active');
	const canvas = dom('#image_to_upload');
	canvas.prop('width', 0);
	canvas.prop('height', 0);
	dom('#btn_save').prop('disabled', true);
	dom('#btn_capture').prop('disabled', true);
}

function delete_image_handler(event) {
	const	$this = dom(event.target);
	const	img_id = $this.attr('data-id');

	if (!isNull(img_id)) {
		AJAX.post({
			'url': '/delete',
			'params': { 'id': img_id },
			'load': function (e) {
				try {
					const	res = JSON.parse(e.target.responseText);
					if (res.error === 0)
						event.target.parentNode.parentNode.remove();
					setMessage(res, '#message');
				}
				catch (e) {

				}
			}
		});
	}
}
